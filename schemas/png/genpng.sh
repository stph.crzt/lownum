#!/bin/bash

# Exporting every SVG to a PNG
cd ..
for f in *.svg; 
do echo $f; 
inkscape "$f" -o png/out/"$f".png; 
done;

# Extracting part of "valeurs" and exporting to PNG

xsltproc png/xsl/valeurs2technosol.xsl lowtechisation-valeurs.svg > png/tmp/lowtechisation-valeurs-technosolutionnisme.svg

xsltproc png/xsl/valeurs2lt.xsl lowtechisation-valeurs.svg > png/tmp/lowtechisation-valeurs-lowtechisation.svg

xsltproc png/xsl/valeurs2lt-with-details.xsl lowtechisation-valeurs.svg > png/tmp/lowtechisation-valeurs-lowtechisation-with-details.svg

inkscape --actions="export-id:technosolutionnisme-group; export-filename:png/out/lowtechisation-valeurs-technosolutionnisme.svg.png; export-do;" png/tmp/lowtechisation-valeurs-technosolutionnisme.svg

inkscape --actions="export-id:lowtechisation-group; export-filename:png/out/lowtechisation-valeurs-lowtechisation.svg.png; export-do;" png/tmp/lowtechisation-valeurs-lowtechisation.svg

inkscape --actions="export-id:lowtechisation-group; export-filename:png/out/lowtechisation-valeurs-lowtechisation-with-details.svg.png; export-do;" png/tmp/lowtechisation-valeurs-lowtechisation-with-details.svg

