<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:svg="http://www.w3.org/2000/svg"
    exclude-result-prefixes="xs"
    version="1.1">
    
    <xsl:template match="svg:g[@id='technosolutionnisme-group']" priority="1"/>
    
    <xsl:template match="svg:g[contains(@id,'-details')]" priority="1"/>
        
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>  
    
</xsl:stylesheet>