#!/bin/bash
for f in *.svg; 
do echo $f; 
inkscape "$f" -o "$f".pdf; 
done;

rm lownum.svg.pdf;
pdfunite *.svg.pdf lownum-fds.svg.pdf;

rm lownum_simple.svg.pdf;
pdfunite *_simple.svg.pdf lownum-fds_simple.svg.pdf;
